import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {HomePublicClientPageComponent} from "./home-public-client-page.component";

const routes: Routes = [
  {
    path:'',
    component: HomePublicClientPageComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HomePublicClientRoutingModule { }
