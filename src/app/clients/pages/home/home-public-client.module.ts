import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HomePublicClientRoutingModule } from './home-public-client-routing.module';
import { HomePublicClientPageComponent } from './home-public-client-page.component';


@NgModule({
  declarations: [
    HomePublicClientPageComponent
  ],
  imports: [
    CommonModule,
    HomePublicClientRoutingModule
  ]
})
export class HomePublicClientModule { }
