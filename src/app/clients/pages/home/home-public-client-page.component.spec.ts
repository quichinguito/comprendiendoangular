import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HomePublicClientPageComponent } from './home-public-client-page.component';

describe('HomePublicClientPageComponent', () => {
  let component: HomePublicClientPageComponent;
  let fixture: ComponentFixture<HomePublicClientPageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HomePublicClientPageComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HomePublicClientPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
