import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PublicClientRoutingModule } from './public-client-routing.module';
import { PublicClientPageComponent } from './public-client-page.component';


@NgModule({
  declarations: [
    PublicClientPageComponent
  ],
  imports: [
    CommonModule,
    PublicClientRoutingModule
  ]
})
export class PublicClientModule { }
