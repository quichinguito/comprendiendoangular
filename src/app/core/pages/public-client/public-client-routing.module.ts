import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {PublicClientPageComponent} from "./public-client-page.component";

const routes: Routes = [
  {
    path:'',
    component: PublicClientPageComponent,
    children: [
      {
        path: 'home',
        children: [
          {
            path: '',
            loadChildren: () => import('../../../clients/pages/home/home-public-client.module').then(m => m.HomePublicClientModule),
          }
        ]
      },
      //anidamiento por routing de diferentes modulos del cliente.
      { path: '', redirectTo: 'home' },
    ],
  },
  ];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PublicClientRoutingModule { }
