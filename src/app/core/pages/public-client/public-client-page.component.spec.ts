import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PublicClientPageComponent } from './public-client-page.component';

describe('PublicClientPageComponent', () => {
  let component: PublicClientPageComponent;
  let fixture: ComponentFixture<PublicClientPageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PublicClientPageComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PublicClientPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
