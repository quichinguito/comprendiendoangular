import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AuthRoutingModule } from './auth-routing.module';
import { AuthPageComponent } from './pages/auth-page/auth-page.component';
import { LoginHandlerComponent } from './containers/login-handler/login-handler.component';
import {SharedAccessModule} from "../shared-access/shared-access.module";
import { LoginFormComponent } from './components/login-form/login-form.component';


@NgModule({
  declarations: [
    AuthPageComponent,
    LoginHandlerComponent,
    LoginFormComponent
  ],
  imports: [
    CommonModule,
    AuthRoutingModule,
    SharedAccessModule,
  ]
})
export class AuthModule { }
