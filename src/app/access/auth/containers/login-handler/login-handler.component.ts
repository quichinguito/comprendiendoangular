import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import Swal from 'sweetalert2'
import {Router} from "@angular/router";
import {MatSnackBar} from "@angular/material/snack-bar";

type User = {
  email: string;
  password: string;
};

@Component({
  selector: 'mav-login-handler',
  templateUrl: './login-handler.component.html',
  styleUrls: ['./login-handler.component.css']
})
export class LoginHandlerComponent implements OnInit {

  user1: User = {
    email: '1@1',
    password: '12345'
  }
  //@Output() loginCredentials = new EventEmitter<any>();

  constructor(private router: Router, private _snackBar: MatSnackBar) { }

  ngOnInit(): void {
  }

  credentialsBasicAuth(credentials: User) {

    if(credentials.email != this.user1.email || credentials.password != this.user1.password){
      this._snackBar.open('Usuario o Contraseña invalida', 'Ok');
    }
      this.router.navigate(['/client']);
  }

  showModalMessageError(field: string){
    /*
    Swal.fire({
      icon: 'error',
      title: 'Error de AutentiFIcación',
      text: `Corrija ${field}`,
    })

     */
    this._snackBar.open(field, 'Ok');

  }
}
