import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";

@Component({
  selector: 'mav-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.css']
})
export class LoginFormComponent implements OnInit {
  loginForm: FormGroup;
  hide: Boolean = true;
  @Output() credentials = new EventEmitter<any>();

  constructor(private fb: FormBuilder) {
    this.loginForm = fb.group({
      userMail: ['', [Validators.required, Validators.email]],
      userPassword: ['', [Validators.required, Validators.pattern('[a-zA-Z_0-9_-_*]{3,16}')]],
    });
  }

  ngOnInit(): void {
  }

  sendDataFormLogin() {
      this.credentials.emit({ email: this.loginForm.controls['userMail'].value, password: this.loginForm.controls['userPassword'].value });
  }

  getErrorMessageEmail() {
    if (this.loginForm.controls['userMail'].hasError('required')) {
      return 'You must enter a email value';
    }
    return this.loginForm.controls['userMail'].hasError('email') ? `${this.loginForm.controls['userMail'].value} Not a valid email` : '';
  }

  getErrorMessagePassword() {

    if (this.loginForm.controls['userPassword'].hasError('required')) {
      return 'You must enter a key value ';
    }
    return this.loginForm.controls['userPassword'].hasError('pattern') ? 'Not a valid key' : '';
  }
}
