import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderAccessComponent } from './components/header-access/header-access.component';
import {AngularMaterialModule} from "../../../../libs/angular_material/angular-material.module";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";



@NgModule({
    declarations: [
        HeaderAccessComponent
    ],
    imports: [
       CommonModule,
       AngularMaterialModule,
       FormsModule,
       ReactiveFormsModule,
  ],
    exports: [
        HeaderAccessComponent,
        AngularMaterialModule,
        FormsModule,
        ReactiveFormsModule,
    ],

})
export class SharedAccessModule { }
