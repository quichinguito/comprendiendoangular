import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RegisterHandlerComponent } from './register-handler.component';

describe('RegisterHandlerComponent', () => {
  let component: RegisterHandlerComponent;
  let fixture: ComponentFixture<RegisterHandlerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RegisterHandlerComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RegisterHandlerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
