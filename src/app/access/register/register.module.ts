import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RegisterRoutingModule } from './register-routing.module';
import { RegisterPageComponent } from './pages/register-page/register-page.component';
import {SharedAccessModule} from "../shared-access/shared-access.module";
import { RegisterHandlerComponent } from './containers/register-handler/register-handler.component';
import { RegisterFormComponent } from './components/register-form/register-form.component';


@NgModule({
  declarations: [
    RegisterPageComponent,
    RegisterHandlerComponent,
    RegisterFormComponent
  ],
  imports: [
    CommonModule,
    RegisterRoutingModule,
    SharedAccessModule,
  ]
})
export class RegisterModule { }
