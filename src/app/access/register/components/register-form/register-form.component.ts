import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";

@Component({
  selector: 'mav-register-form',
  templateUrl: './register-form.component.html',
  styleUrls: ['./register-form.component.css']
})
export class RegisterFormComponent implements OnInit {
  registerForm: FormGroup;
  hide: Boolean = true;
  @Output() dataRegister = new EventEmitter<any>();
  constructor(private fb: FormBuilder) {

    this.registerForm = fb.group({
      userName: ['', [Validators.required, Validators.minLength(3), Validators.pattern('[a-zA-Z]')]],
      userMail: ['', [Validators.required, Validators.email]],
      userPassword: ['', [Validators.required, Validators.pattern('[a-zA-Z_0-9_-_*]{3,16}')]],
      confirmPassword: ['', [Validators.required, Validators.pattern('[a-zA-Z_0-9_-_*]{3,16}')]],
    });
  }

  ngOnInit(): void {
  }

  sendDataFormRegister() {

  }

  getErrorMessageName() {
    if(this.registerForm.controls['userName'].hasError('required')){
      return 'You must enter a user name value';
    }else if(this.registerForm.controls['userName'].hasError('pattern')){
      return 'Not a valid user name,  only enter words'
    }
    return this.registerForm.controls['userName'].hasError('minLength') ? 'Not a valid user, minimum 3 characters' : '';

  }
}
