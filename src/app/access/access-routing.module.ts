import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {AccessPageComponent} from "./pages/access-page/access-page.component";

const routes: Routes = [
  {
    path: '',
    component: AccessPageComponent,
    children: [
      {
        path: 'auth',
        children: [
          {
            path: '',
            loadChildren: () => import('../access/auth/auth.module').then(m => m.AuthModule)
          }
        ]
      },
      {
        path: 'register',
        children: [
          {
            path: '',
            loadChildren: () => import('../access/register/register.module').then(m => m.RegisterModule)
          }
        ]
      },
      { path: '', redirectTo: 'auth' },
    ],
  }

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AccessRoutingModule { }
