import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AccessRoutingModule } from './access-routing.module';
import { AccessPageComponent } from './pages/access-page/access-page.component';
import {SharedAccessModule} from "./shared-access/shared-access.module";


@NgModule({
  declarations: [
    AccessPageComponent,
  ],
  imports: [
    CommonModule,
    AccessRoutingModule,
    SharedAccessModule,
  ],
})
export class AccessModule { }
