import {NgModule} from "@angular/core";
import {RouterModule, Routes} from "@angular/router";


const routes: Routes = [
  {
    path: '',
    redirectTo: 'access',
    pathMatch: 'full',
  },
  {
    path: 'access',
    loadChildren: () => import('./access/access.module').then(m=> m.AccessModule),
  },
  {
    path: 'client',
    loadChildren: () => import('./core/pages/public-client/public-client.module').then(m => m.PublicClientModule),
  },
  {
    path: '**',
    redirectTo: 'access',
  },];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule{}
